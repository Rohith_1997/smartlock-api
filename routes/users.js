const { User, validate } = require('../models/user');
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
router.get('/', async (req, res) => {
    // First Validate The Request
    /*const { error } = validate(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }*/
 
    // Check if this user already exisits
    let user = await User.findOne({ email: req.query.email });
    if (user) {
	console.log("got a registration request");
        return res.status(400).send('That user already exisits!');
    } else {
        // Insert the new user if they do not exist yet
	console.log("got a registration request");
        user = new User({
            name: req.query.name,
            email: req.query.email,
            password: req.query.password
        });
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(user.password, salt);
        await user.save();
        res.send("true");
    }
});
 
module.exports = router;
